#!/usr/bin/env python
# coding: UTF-8
import fontforge
import os

release = "0.02"
Flags = ("PfEd-lookups","opentype")

def generate(file):
	font = fontforge.open(file)
	#font.selection.select("U+0622")
	#font.copyReference()
	#font.selection.select(("ranges", None), "U+FE80", "U+FEFC")
	#font.paste()
	fontname = font.fontname + ".ttf"
	font.generate(fontname, flags=Flags)
	font.close()

def obliqize(infont, outfont, angle, family, arfamily):
	font = fontforge.open(infont)
	import psMat
	skew = psMat.skew(angle)
	font.selection.select("U+0021","U+0028","U+0029","U+005B","U+005D","U+007B","U+007D")
	font.selection.invert()
	font.unlinkReferences()
	font.transform(skew)
	font.familyname = font.fontname = font.fontname + "-Oblique"
	font.fullname = font.fullname + " Oblique"
	font.appendSFNTName('English (US)', 'SubFamily', family)
	font.appendSFNTName('Arabic (Egypt)', 'SubFamily', arfamily)
	font.save(outfont)
	font.close()


def build():
	# reglar
	generate("Mothanna.sfd")
	# bold
	generate("Mothanna-Bold.sfd")

	norm = "/tmp/MothannaI.sfd"
	bold = "/tmp/MothannaBI.sfd"
	# oblique
	obliqize("Mothanna.sfd", norm, -16, "Oblique", "مائل")
	generate(norm)
	# bold-oblique
	obliqize("Mothanna-Bold.sfd", bold, -16, "Bold Oblique", "عريض مائل")
	generate(bold)

	thb = "Mothanna-"+release
	os.system("mkdir -p %s" % thb)
	os.system("mv *.ttf %s" % thb)
	os.system("cp OFL.txt ChangeLog README* %s" % thb)

if __name__ == '__main__':
	build()
